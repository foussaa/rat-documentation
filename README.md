![Logo](./figs/RATLogo.png)
# Documentation - This README is work in progres
<em>Hellooo nice to meet you. Rat is happy you visit. Rat put together nice coil for you yes?</em>

## Content
1. [Introduction](#introduction) \
2. [Requirements](#requirements) \
3. [General installation notes](#installation) \
4. [ToDo list](#todo_list) \
5. [Contribution](#contribution) \
6. [Versioning](#versioning) \
7. [Authors](#authors) \
8. [License](#license) \
9. [Acknowledgements](#acknowledgements) \
10. [References](#references)


## Introduction
Software for the design of coils and magnets is difficult to come by. Commercial FEM packages often offer magneto-static solvers. However, their general nature often makes it difficult to setup complex coil geometries. In addition, the required meshing of the air domain can result in a very high number of elements, causing the computation to use a very large amount of memory and time. Many universities and institutes have developed their own tools and workarounds over the years, but only very few of them are publicly available, even though their development was funded with public money. 

Rat (Rot A in Tesla) attempts to fill this gap by supplying a set of Open Source libraries for modeling coil geometries and their respective field calculation. Included are a Multi-Level Fast Multipole Method \[1\] (MLFMM) for calculating Vector Potential and Magnetic Field as function of any set of line current elements and a coil modeler that allows modeling almost any coil geometry. It must be noted that this code is still under development and thus some issues can be expected. However, it could be a good starting point to develop into a general set of Open-Source magnet design and simulation tools with and for the community.

![mini_toroid](./figs/mini_toroid2.png)

## Requirements
Rat is currently being developed in Linux (Arch) and OSx (Mojave). Although all required external libraries are in principle also available for Windows as well, expect some trouble when using this platform. To install the required packages in Linux one can simply use the respective package manager (or manually if you like extra work). In OSx it is recommended to use [MacPorts](https://www.macports.org/). The following packages are used for Rat:
* [GCC](https://gcc.gnu.org/)/[CLANG](https://clang.llvm.org/) - **Required** - To compile the code you will need a C++ compiler. The code was tested with both GCC9 and CLANG. 
* [CMAKE](https://cmake.org/) - **Required** - A CMAKE build system is used to simplify compilation and linking of the various parts of the code.
* [BOOST](https://www.boost.org/) - **Required** - Needed for interaction with the filesystem.
* [MAKE](https://www.gnu.org/software/make/) - **Required** - After setting up with CMAKE you will need make to run the compilation.
* [SuperLU](https://github.com/xiaoyeli/superlu) - **Required** - Required for Armadillo to allow solving linear systems of equations of the form Ax=b. In some cases it needs to be installed manually.
* [Armadillo](http://arma.sourceforge.net) - **Required** - This library takes care of all linear algebra operations [2]. It is effectively a wrapper around [BLAS](http://www.netlib.org/blas/). Armadillo comes with its own lightweight implementation, but it is recommended to link it against more performant libraries, such as [OpenBLAS](https://www.openblas.net/), [ATLAS](http://math-atlas.sourceforge.net/) and [Intel-MKL](https://software.intel.com/en-us/mkl). Refer to the Armadillo manual for more information on linking to BLAS. Note that since Rat uses Posix threads to parallelize the code, it is necessary to use BLAS in single threaded mode. This can be achieved by setting the respective environment variable OMP_NUM_THREADS or MKL_NUM_THREADS to one using: export OMP_NUM_THREADS=1.
* [VTK](https://vtk.org/) - **Required** - The visualization toolkit (VTK) is required for writing output files to VTK format. This part is unfortunately inseperable from the rat-models library and is therefore required for it.
* [Jsoncpp](http://open-source-parsers.github.io/jsoncpp-docs/doxygen/index.html) - **Required** - The json format is used for serializing user objects in order to store and load models to and from text files. 
* [TClap](http://tclap.sourceforge.net) - **Required** - A command line input parser used for writing small, light weight, command line applications.
* [Paraview](https://www.paraview.org/) - **Optional** - Paraview is currently used as the main post processing tool for [rat-models](https://gitlab.com/Project-Rat/rat-models) and is highly recommended when this part of Rat is used. Paraview can in some cases be in conflict with VTK. Most package managers offer Paraview-opt as a work-around. 
* [GMSH](http://gmsh.info/) - **Optional** - As alternative to Paraview the output data can also be written in GMSH format. This is at present more limited and the Paraview route is preferred.
* [NVIDIA CUDA](https://developer.nvidia.com/cuda-zone) - **Optional** - In rat-mlfmm it is possible to run the most compute heavy kernels (M2L and S2T) on the GPU using NVIDIA CUDA. In order for this to run one needs a CUDA capable graphics card with compute capability 7.0 or more (not free) and the CUDA development toolkit (freely available from NVIDIA and in most package managers).
* [FreeCAD](https://www.freecadweb.org/) - **Optional** - In order to export the coil geometries to a proper CAD file such that it can be edited, the geometry is reconstructed with a Macro (script) in FreeCAD. This allows saving the coils, even complex CCT ones, as a step file.
* [GIT](https://gitlab.com/) - **Optional** - To download the repository one can clone it from GIT. As it is also possible to download and unzip the code manually, it is not necessary to have GIT installed. However, when actively contributing to the code it will be required, as it is our version control system. 

## Installation
This section describes the general installation process. Platform specific installation guides are available here:
* [Installation on CentOS 8 (CERN)](linux_cc8_install.md)
* [Installation on MacOS](osx_install.md)

Rat consists of different libraries that are each installed separately. The libraries have inter-dependencies and thus it is important to install them in the order given below. Note that its only necessary to install up-to the item you wish to use. The installation procedure is usually the same, but please find more detailed information in the respective repositories by following the links. The different Rat libraries are:
* [Rat-Common](https://gitlab.com/Project-Rat/rat-common) - This library contains common files useful for all different parts of Rat. It contains, amongst other things, extensions to Armadillo, several iterative solvers, a logger and error handling. 
* [DistMesh-CPP](https://gitlab.com/Project-Rat/distmesh-cpp) - A two-dimensional tri- and quad-mesher originally written in Matlab by P.-O. Persson. In contrast to most usual meshers it uses a distance function to describe the geometry. Rat has its own version of the mesher translated to C++ augmented with Armadillo.
* [Materials-CPP](https://gitlab.com/Project-Rat/materials-cpp) - Material property library based on json files with modeling integration. 
* [Rat-MLFMM](https://gitlab.com/Project-Rat/rat-mlfmm) - The Multi-Level Fast Multipole Method used for calculating the vector potential and magnetic field from any collection of line current elements or magnetic moments. This is the heart of Rat and is not only useful for magneto-static coil modeling, but can for example also be used for calculating inductive voltages in a network simulation. It is therefore provided as a separate module.
* [Rat-Models](https://gitlab.com/Project-Rat/rat-models/) - This library allows for modeling coils and to calculate their magnetic fields, for example, on the surface of the coil or in a volume. Many tools related to magnet design reside here. 
* [Rat-NL](https://gitlab.com/Project-Rat/rat-nl) - This is the repository for the future non-linear materials solver, which is not yet implemented. The idea is to build an A-edge integral method, using the MLFMM to avoid the fully dense matrix, usually associated with integral methods.

Each library is installed using the following steps. First in the terminal browse to the directory where you wish to store the source code and build files. I.e. 
```
cd ~/Projects
```
Then clone or download the repository using
```
git clone X
```
where X is the SSH (preferred if setup) or HTTPS link to the repository. When the code is downloaded use an out-of-core (separate directory) build using 
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=T -DCMAKE_INSTALL_PREFIX=Y ..
make -jX
```
where X is the number of cores you wish to use for the build and T is the build type, either "Release" or "Debug", depending on whether you wish to use extra error checking, which is slower but more safe, especially when actively developing. Y is the install prefix this is usually "/usr/local" (default) on Linux and "/opt/local" for MacOs (macports). This determines where the library will be installed. After building run
```
make test
```
to run a series of tests that check the build. Then finally, to install the library run
```
make install
```
which copies the library and respective header files to your install directory (Y) where it can be found by other parts of the code. It could be necessary to run this final step as superuser (sudo), as this directory may not have user rights. Repeat these steps for each repository.

## Search Path and Environment Variables
To ensure efficient multi-threading it is necessary to use single threaded BLAS. This is controlled by environment variables and depends on which library you are using. For OpenBlas you set
```
export OMP_NUM_THREADS=1
```
and for Intel MKL
```
export MKL_NUM_THREADS=1
```
In order to correctly link to Intel-MKL it may be necessary to add its library location to the search path:
```
export PATH=$PATH:/opt/intel/mkl/lib/intel64
```
When using CUDA to allow cmake to find the NVCC compiler you can add:
```
export PATH=$PATH:/opt/cuda/bin
```
It is possible to type these commands directly in the terminal prior to running Rat. But to make it persistent you can append them to your .bashrc file or .zshrc file (depending on which shell your linux distribution uses), which are usually located in the home directory. Note that these files are hidden.

## Versioning
* v2.012.0 - Added version header file and rework of material properties database.
* v2.010.0 - The code is re-issued under MIT license.
* v1.100.0 - The code now allows compiling with single or double precision using the ENABLE_DOUBLE_PRECISION setting in Rat-Common (change with ccmake). The other libraries automatically adapt to this setting. To ensure the correct floating point type, Rat supplies type definition for it named rat::fltp. Floating point constants should be set using the macro RAT_CONST(x) to automatically append "f" when using single precision. The precision of the GPU CUDA code in the MLFMM can be set separately and automatic conversion between double and float is applied when necessary. However, the conversion causes some overhead causing the code to run slower. In Rat-Models the meshes now derive from the MLFMM sources and targets and can be directly used in a calculation. This avoids copying the mesh and thereby reduces memory overhead. This also allows the VTK file output to store each mesh into a separate file. A Paraview Data file is added, which automatically loads all the meshes into Paraview. Time is no longer set in the calculation (now "ModelMlfmm"). Instead the meshes are re-created each time step. This allows for greater flexibility in the code. Most calculations can be performed using the "rat::mdl::ModelMlfmm" and "rat::mdl::ModelInductance" classes, which act as wrappers. To avoid compilation problems the code is compiled in C++11. Communication with the file-system is now done through Boost, a new dependency.
* v1.000.x - This is the original code that came out of Alpha.

## ToDo List
Not available.

## Contribution
Not yet.

## Authors
* Jeroen van Nugteren
* Nikkie Deelen

## Acknowledgements
* Dr. Conrad Sanderson and Dr. Ryan Curtin are acknowledged for creating the [Armadillo](http://arma.sourceforge.net) library which is extensively used throughout the project.
* Kitware is acknowledged for providing excellent Open-Source visualisation packages [VTK](https://vtk.org/) and [Paraview](https://www.paraview.org/).
* Per-Olof Persson is acknowledged for implementing the origininal distmesh code and allowing me to re-implement it in C++.
* Dr. Nikkie Deelen is acknowledged for writing the documentation and rat support on MacOS.
* Saman Ghannadzadeh is acknowledged for helping improve the CMake system and making it compatible for debian based systems.
* Dr. Antti Stenvall is acknowledged for his advice on the graphical user interface.
* Dr. Bernhard Auchmann is acknowledged for his advice on the constant perimeter coil ends.
* Thomas Nes is acknowledged for writing a [manual](doc/RAT_installation_guide.pdf) for installing Rat in a virtual box machine. 
* [CERN](https://home.cern/) is acknowledged for supporting me in 2019 durin the development for the first version of Rat (version alpha).

## License
This project is licensed under the [MIT](LICENSE).

## References
[1] L. Greengard and V. Rokhlin. A Fast Algorithm for Particle Simulations. J. Comput. Phys. 73, 325–348 (1987).

[2] Conrad Sanderson and Ryan Curtin. Armadillo: a template-based C++ library for linear algebra. Journal of Open Source Software, Vol. 1, pp. 26, 2016.

[3] P.-O. Persson, G. Strang, "A Simple Mesh Generator in MATLAB"
SIAM Review, Volume 46 (2), pp. 329-345, June 2004.

[4] P.-O. Persson, "Mesh Generation for Implicit Geometries",
Ph.D. thesis, Department of Mathematics, MIT, Dec 2004.

[5] P.D. Bourke, "A Contouring Subroutine", BYTE Magazine, June, 1987.