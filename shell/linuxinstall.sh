cd ../../
git clone git@gitlab.com:Project-Rat/rat-common.git
cd rat-common
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/distmesh-cpp.git
cd distmesh-cpp
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/materials-cpp.git
cd materials-cpp
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/materials-data.git
cd materials-data
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-mlfmm.git
cd rat-mlfmm
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-models.git
cd rat-models
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Raccoon/raccoon.git
cd raccoon
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Raccoon/raccoon2.git
cd raccoon2
git checkout dev
mkdir build
cd build
cmake ..
make -j64
make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-gui.git
cd rat-gui
git checkout dev
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make -j64
make test
# sudo make install
cd ../../

cd rat-documentation/shell
