cd ../../
git clone git@gitlab.com:Project-Rat/rat-common.git
cd rat-common
git checkout dev
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j11
make test
sudo make install
cd ../../
git clone git@gitlab.com:Project-Rat/distmesh-cpp.git
cd distmesh-cpp
git checkout dev
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j11
make test
sudo make install
cd ../../
git clone git@gitlab.com:Project-Rat/rat-mlfmm.git
cd rat-mlfmm
git checkout dev
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j11
make test
sudo make install
cd ../../
git clone git@gitlab.com:Project-Rat/materials-cpp.git
cd materials-cpp
git checkout dev
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j11
make test
sudo make install
cd ../../
git clone git@gitlab.com:Project-Rat/rat-models.git
cd rat-models
git checkout dev
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j11
make test
sudo make install
cd ../../
cd rat-documentation/shell