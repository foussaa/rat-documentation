cd ../../
cd rat-common
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
sudo make install
cd ../../

cd materials-cpp
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
sudo make install
cd ../../

cd materials-data
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
sudo make install
cd ../../

cd distmesh-cpp
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
sudo make install
cd ../../

cd rat-mlfmm
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
sudo make install
cd ../../

cd rat-models
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
sudo make install
cd ../../

cd raccoon2
git checkout dev
git pull origin dev
sudo rm -r build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
sudo make install
cd ../../

cd rat-gui
git checkout dev
git pull origin dev
git submodule update --recursive --remote
sudo rm -r build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j64
make test
cd ../../
cd rat-documentation/shell


