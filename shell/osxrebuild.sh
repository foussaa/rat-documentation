cd ../../
cd rat-common
git pull origin dev
sudo rm -r release
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j4
sudo make install
cd ../../
cd distmesh-cpp
git pull origin dev
sudo rm -r release
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j4
sudo make install
cd ../../
cd rat-mlfmm
git pull origin dev
sudo rm -r release
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j4
sudo make install
cd ../../
cd rat-models
git pull origin dev
sudo rm -r release
mkdir release
cd release
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
make -j4
sudo make install
cd ../../
# cd elmath2
# git pull origin dev
# sudo rm -r release
# mkdir release
# cd release
# cmake ..
# make -j11
# sudo make install
# cd ../../
cd rat-documentation/shell
